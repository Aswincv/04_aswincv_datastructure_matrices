/*
Name of the project : Datastructure_matrices
File name : matrices_1/display.js
Description : prints the array in spiral form
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Input : [[0,3,1,0],[3,0,3,3],[2,3,0,3],[0,3,3,3]],4,4
Output : 0, 3, 2, 0, 3, 3, 3, 3, 3, 0, 2, 3, 0, 3, 0, 3
*/


var result = [];
function spiralPrint(arr,m,n) {
    var i, k = 0, l = 0;
	  while (k < m && l < n) {
		 /* Print the first row from the remaining rows */
		    for (i = l; i < n; ++i) {
			       result.push(arr[i][k]);
	    	 }
		     k++;
	      	/* Print the last column from the remaining columns */
		     for (i = k; i < m; ++i) {
			       result.push(arr[i][n-1]);
		     }
		     n--;
        	/* Print the last row from the remaining rows */
		     if ( k < m) {
			       for (i = n-1; i >= l; --i) {
				         result.push(arr[m-1][i]);
			        }
			    m--;
		      }
         	/* Print the first column from the remaining columns */
	       	if (l < n) {
			        for (i = m-1; i >= k; --i) {
				          result.push(arr[i][l]);
			        }
			l++;
		}
	}
}
spiralPrint([[0,3,1,0],[3,0,3,3],[2,3,0,3],[0,3,3,3]],4,4);
console.log(result);
