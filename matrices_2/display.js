/*
Name of the project : Datastructure_matrices
File name : matrices_2/display.js
Description : checks the given sudoku pattern is correct or not.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Input : [[3,0,6,5,0,8,4,0,0]
       ,[5,2,0,0,0,0,0,0,0],[0,8,7,0,0,0,0,3,1]
       ,[0,0,3,0,1,0,0,8,0],[9,0,0,8,6,3,0,0,5]
       ,[0,5,0,0,9,0,6,0,0],[1,3,0,0,0,0,2,5,0]
       ,[0,0,0,0,0,0,0,7,4],[0,0,5,2,0,6,3,0,0]]
Output : valid
*/


function find(arr) {
    var flag = false;
    for (var i = 0; i < 9; i++) {
        for (var j = 0; j < 9; j++) {
            if( arr[i][j] != 0 ) {
                if( !check(arr,i,j) ) {
                    flag = true;
                    console.log("invalid");
                    return;
                }
            }
        }
    }
    console.log("valid");
}


function check(arr,x,y) {
     for (var i = 0; i < 9; i++) {
         if( i != y ) {
             if( arr[x][i]==arr[x][y] )
             return -1;
         }
         if( i != x ) {
           if( arr[i][y]==arr[x][y] )
           return -1;
         }
     }
     return 1;
}


find([[3,0,6,5,0,8,4,0,0]
  ,[5,2,0,0,0,0,0,0,0],[0,8,7,0,0,0,0,3,1]
  ,[0,0,3,0,1,0,0,8,0],[9,0,0,8,6,3,0,0,5]
  ,[0,5,0,0,9,0,6,0,0],[1,3,0,0,0,0,2,5,0]
  ,[0,0,0,0,0,0,0,7,4],[0,0,5,2,0,6,3,0,0]]);
